# install.packages("dplyr")

require(dplyr)


dat.enrollment <- read.csv(
  file = "2016-17 enrollment by race raw.csv",
  header = T,
  na.strings = c("**")
)

dat.enrollment.groupByDistrictSchool <- group_by(
  .data = dat.enrollment,
  District..,
  District,
  School..,
  School
)

dat.enrollment.summarized <- summarize(
  .data = dat.enrollment.groupByDistrictSchool,
  BlackEnrollment = sum(Black.or.African.American, na.rm = T),
  TotalEnrollment = sum(TOTAL, na.rm = T)
)

dat.enrollment.summarized.WithBlack <- filter(
  .data = dat.enrollment.summarized,
  is.na(BlackEnrollment)==F
)
dat.enrollment.summarized.WithBlack$PercentBlack <- dat.enrollment.summarized.WithBlack$BlackEnrollment / dat.enrollment.summarized.WithBlack$TotalEnrollment

dat.enrollment.summarized.WithBlack.Broward <- filter(
  .data = dat.enrollment.summarized.WithBlack,
  District=="BROWARD"
)


dat.punishment <- read.csv(
  file = "2016-17 punishment raw.csv",
  header = T,
  na.strings = c('*')
)

dat.punishment.groupBySchoolDistrict <- group_by(
  .data = dat.punishment,
  District,
  District.Name,
  Discipline.School,
  Discipline.School.Name
)

dat.punishment.summarized <- summarize(
  .data = dat.punishment.groupBySchoolDistrict,
  BlackStudentsPunished = sum(Black.or.African.American, na.rm = T),
  TotalPunished = sum(Total, na.rm = T)
)
dat.punishment.summarized$BlackStudentsPercentOfPunished <- dat.punishment.summarized$BlackStudentsPunished / dat.punishment.summarized$TotalPunished

dat.punishment.summarized.Broward <- filter(
  .data = dat.punishment.summarized,
  District.Name=="BROWARD",
  TotalPunished>0
)


dat.merge <- merge(
  x = dat.enrollment.summarized.WithBlack.Broward,
  y = dat.punishment.summarized.Broward,
  by.x = "School..",
  by.y = "Discipline.School"
)
dat.merge <- filter(
  .data = dat.merge,
  dat.merge$PercentBlack < 1,
  dat.merge$PercentBlack > 0,
  dat.merge$TotalPunished < dat.merge$TotalEnrollment
)
dat.merge$PercentOfBlackStudentsPunished <-dat.merge$BlackStudentsPunished / dat.merge$BlackEnrollment
dat.merge$PercentOfNonBlackStudentsPunished <- dat.merge$TotalPunished / dat.merge$TotalEnrollment
dat.merge$PunishmentDisparity <- dat.merge$PercentOfBlackStudentsPunished - dat.merge$PercentOfNonBlackStudentsPunished

dat.cor <- cor(
  x = dat.merge[, sapply(dat.merge,is.numeric)],
  method = "pearson"
)

plot(
  x = dat.merge$PercentOfNonBlackStudentsPunished,
  y = dat.merge$PercentOfBlackStudentsPunished
)

plot(
  x = dat.merge$PercentBlack,
  y = dat.merge$PercentOfBlackStudentsPunished
)

plot(
  x = dat.merge$PercentBlack,
  y = dat.merge$PercentOfNonBlackStudentsPunished
)